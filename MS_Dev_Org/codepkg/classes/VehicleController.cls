public class VehicleController{

    public VehicleController(ApexPages.StandardController controller) {

    }


    public String getAccount() {
        return null;
    }

    public Vehicle Vehicle{ get; set; }

    public String Records { get; set; }

    public String getMessage() {
        return 'Vehicle is retrieved';
    }

    public PageReference GoBack() {
        return null;
    }

    public PageReference linkIt() {
        return null;
    }
    
    public static List<Vehicle> GetVehiclesByPolicy() {
         try {
           string policyId = apexpages.currentPage().getParameters().get('id') ;  
           if(policyId!= null)
           {
            List<Vehicle> vehicles =  new List<Vehicle>();
            Vehicle v = new Vehicle();
     
            v.Customer360PolicyID = 13456;
            v.Year = 2012;
             v.Make = 'Nissan';
            v.Model = 'Altima';
             v.InsuredValue= 3500;
            v.VehicleType = 'Auto';
           vehicles.Add(v);
             v = new Vehicle();
            v.Customer360PolicyID = 23456;
            v.Year = 2013;
             v.Make = 'Nissan';
            v.Model = 'Altima';
            v.InsuredValue= 2500;
            v.VehicleType = 'Auto';
            vehicles.Add(v);
             v = new Vehicle();
            v.Customer360PolicyID = 33456;
            v.Year = 2014;
              v.Make = 'Nissan';
            v.Model = 'Altima';
             v.InsuredValue= 2000;
            v.VehicleType = 'Auto';
           vehicles.Add(v);
            return vehicles;  
            }
            return null;               
         } 
         catch(System.DMLException e) 
         {
            ApexPages.addMessages(e);
            return null;
        }
     
        }
          
     public class Vehicle 
    {
        public integer Customer360PolicyID { get; set; }
        public string VehicleType { get; set; }
        public integer Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public decimal InsuredValue { get; set; }
        public integer ModifiedInd { get; set; }
        public string RateAttribute { get; set; }     
        
    }
}