public class VFFileUpload  
{  
Private Apexpages.StandardController controller;
Employee__c sObj;

    public Id id  
    {    get;set;    }  
      
   public VFFileUpload(ApexPages.StandardController stdcontroller) {
        id = ApexPages.currentPage().getParameters().get('id');
        this.controller = stdController;   
        sObj = (Employee__c)stdController.getRecord();    
    }  
      
    transient public string fileName   
    {    get;set;    }  
      
    transient public Blob fileBody   
    {    get;set;    }
    
     public PageReference save(){ 
        try{
                this.controller.save(); 
                UploadFile();
                return null;    
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
            return null;
        }
    }  
    
    public void UploadFile()  
    {  
     System.debug('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');     
        PageReference pr;  
        if(fileBody != null && fileName != null)  
        {  
          Attachment myAttachment  = new Attachment();  
          myAttachment.Body = fileBody;  
          //myAttachment.Name = fileName;  
          
          myAttachment.Description = fileName;
           System.debug('fileBody:::::::::::' + filebody);     
           System.debug('filename:::::::::::' + filename);
          myAttachment.ParentId = this.controller.getId();   
          insert myAttachment;  
        } 
    }      
    }