public with sharing class ApplicationUtility 
{
    public static Boolean overrideTriggerExecution(String objectName)
    {
      Override_Triggers__c overrideTrigger = Override_Triggers__c.getInstance(UserInfo.getUserId());
      if (overrideTrigger != null && overrideTrigger.Id != null)
      {
        if (Boolean.valueOf(overrideTrigger.get('Override_' + objectName + '_Trigger__c')))
        {
          /*
          Set<Id> objectRecordsToExclude = new Set<Id>();
          if (overrideTrigger.get(objectName + '_Record_IDs__c') != null)
          {
            objectRecordsToExclude = overrideTrigger.get(objectName + '_Record_IDs__c').Split('\r\n');
            
          }
          else
          {
            return true;
          }*/
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        return false;
      }
    }
    }