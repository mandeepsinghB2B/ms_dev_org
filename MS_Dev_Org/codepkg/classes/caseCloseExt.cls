public class caseCloseExt{

    
    public List<CaseWrap> wrappers {get; set;}

    public caseCloseExt(ApexPages.StandardSetController controller) {
    
        wrappers=new List<CaseWrap>();
        
        Set<Id> ids = new Set<Id>();

        for(Case c : (list<Case>)controller.getSelected()){
            ids.add(c.id);    
        }
        
        List<Case> cases = [SELECT Id, CaseNumber, Status, Subject, ContactId, accountId FROM Case WHERE Id IN :ids]; //(list<Case>)controller.getSelected();
        for(Case c : cases){
            CaseWrap wrap = new CaseWrap(c);
            wrappers.add(wrap);
        }
        
    }
   
    public list<SelectOption> getClosedStatuses(){

        List<SelectOption> options = new list<SelectOption>();
        List<CaseStatus> statuses = [Select c.SortOrder, c.MasterLabel, c.IsClosed, c.CreatedDate From CaseStatus c where c.isclosed = true ORDER BY c.SortOrder];
           
        for(CaseStatus cs : statuses){
            options.add(new SelectOption(cs.MasterLabel, cs.MasterLabel)); 
        }
        return options;
    }

    
    /*public list<SelectOption> getLocales(){
        return getClosedStatuses();
    }*/
   
    public PageReference Save(){

        List<Case> case_list = new List<Case>();
        
        try{
            for (CaseWrap cw : wrappers){
                cw.c.Status = cw.selVal;
                if(cw.c.Id != null){
                    //update cw.c;
                    case_list.add(cw.c);
                }
                
            }
                update case_list;
        }catch(Exception e){
            //ApexPages.addMessages(e);
            system.debug('exception: ' + e.getMessage() + e.getStackTraceString());
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,' Please update Credit Status on Credit Alowed before closing the Case.'));
            return null;
        }

        return new PageReference(ApexPages.CurrentPage().getParameters().get('retURL'));
    }

}