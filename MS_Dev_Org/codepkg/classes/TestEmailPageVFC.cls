public with sharing class TestEmailPageVFC {

  public String toAddress { get; set; }
  public String fromAddress { get; set; }
  public String fromDisplayName { get; set; }

  public TestEmailPageVFC() {}

  public PageReference sendTestEmail() {

    try {
      Messaging.SingleEmailMessage finalMsg = new Messaging.SingleEmailMessage();
      finalMsg.setToAddresses(new List<String>{toaddress});
      
      finalMsg.setSubject('TEST Testing from email address');
      finalMsg.setPlainTextBody('TEST Testing from email address');
      finalMsg.setSenderDisplayName(fromDisplayName);
      finalMsg.setReplyTo(fromAddress);

//finalMsg.setOrgWideEmailAddressId('0D290000000XZKb');

      system.debug('DEVDEBUG From address: ' + fromAddress);

      Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{finalMsg});
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Success'));
      return null;

    } catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
      return null;
    }
  }
}