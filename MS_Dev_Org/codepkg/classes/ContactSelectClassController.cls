public class ContactSelectClassController{
 
   
    public List<wrapAccount> wrapAccountList {get; set;}
    public List<Contact> selectedAccounts{get;set;}
    public String feedid;
    private  List<Id> contactids=new list<Id>();
    public List<Contact> conts;
   
    // public List<Feedback_Contacts__c> feedcon{get;set;} 
    public ContactSelectClassController()
    {
     feedid= (ApexPages.currentPage().getParameters().get('fid'));
    system.debug('*********************Feedback id is :*********************************'+feedid);
     system.debug('*********************contactidscontactidscontactids:*********************************'+selectedAccounts);
           
        if(wrapAccountList == null)
         {
            wrapAccountList = new List<wrapAccount>();
            for(Contact a: [select Id, Name,Email,Phone from Contact])
             {
                wrapAccountList.add(new wrapAccount(a));
             }
         }
     }
   public pageReference CancelAction()
  {
    PageReference pr1 = new PageReference('/apex/LaunchPage');
   
    pr1.setRedirect(false);
    return pr1;
  } 

    public void processSelected()
     {
     
     feedid= (ApexPages.currentPage().getParameters().get('id'));
     system.debug('*********************Feedback id is :*********************************'+feedid);
        
    selectedAccounts = new List<Contact>();
 
        for(wrapAccount wrapAccountObj : wrapAccountList)
         {
            if(wrapAccountObj.selected == true)
             {
                selectedAccounts.add(wrapAccountObj.acc);
                 
             }
         }
          
          for(Contact cont: selectedAccounts)
        {
           contactids.add(cont.Id);
        } 
        
        
        /*feedcon=new List<Feedback_Contacts__c>();
        for(Contact cont: selectedAccounts)
        {
        Feedback_Contacts__c fc=new Feedback_Contacts__c();
        fc.FeedbackContactName__c=cont.Id;
        fc.FeedBackNo__c=feedid;
        
        feedcon.add(fc);
        } 
         insert feedcon;*/
        Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
        mail.setTargetObjectIds(contactids);
        mail.setTemplateId('00X90000001bDY9');
        Messaging.sendEmail(new Messaging.MassEmailMessage[] {mail});
     }
 
    public class wrapAccount {
        public Contact acc {get; set;}
        public Boolean selected {get; set;}
 
        public wrapAccount(Contact a) {
            acc = a;
            selected = false;
        }
    }
}