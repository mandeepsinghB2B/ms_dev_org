public class EventTriggerHandler {
  /**
   ** Perform rollups
  **/
  //public static Map<Integer, FiscalYearSettings> fiscalYearCache = new Map<Integer, FiscalYearSettings>();
  
  public static void onBefore(Map<Id,Event> oldMap, List<Event> events){
    
    //Debug
    Id LV_FirstEventId = events.get( 0 ).Id;
    System.debug( 'LV_FirstEventId: ' + LV_FirstEventId );
     List < Event > LL_DebugEvents = [SELECT Id, Subject FROM Event WHERE Id = :LV_FirstEventId];
    System.debug( 'LL_DebugEventsSize: ' + LL_DebugEvents.size() );
    System.debug( 'LL_DebugEvents: ' + LL_DebugEvents );
    }
}