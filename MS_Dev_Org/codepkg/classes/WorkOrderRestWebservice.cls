@RestResource(urlMapping='/WorkOrder/*')
global with sharing class WorkOrderRestWebservice {

  global class ResponseHandler {
    public String Status    {get;set;}
    public List<String> Data  {get;set;}
    public String Message    {get;set;}
    public String ErrorCode    {get;set;}
  }

  @HttpGet
  global static ResponseHandler GET() {
    ResponseHandler response = new ResponseHandler();

    Map<String, Schema.FieldSet> fsMap = Schema.SObjectType.Account.fieldSets.getMap();
    List<String> fields = new List<String>();

    for (Schema.FieldSetMember fsm : fsMap.get('Field_Set_For_Country1_State').getFields()) {
      fields.add(fsm.getFieldPath());
    }

    if (fields.size() > 0) {
      response.Status = 'Success';
      response.ErrorCode = '';
      response.Data = fields;
      response.Message = 'Success : Found Demo Mobile App Field Set';
    } else {
      response.Status = 'Error';
      response.ErrorCode = '404';
      response.Message = 'Error: could not find field set';
    }

    return response;
  }
}