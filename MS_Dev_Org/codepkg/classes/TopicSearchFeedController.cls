Public Class TopicSearchFeedController{

   
  

        Set<ID> feedItemIds = new Set<ID> ();
        
        Public String topicName  {get;set;}
        Public String typeofFeed {get;set;}
        
        
        Public void searchFeeds(){
        
            For ( TopicAssignment t : [ Select EntityId,EntityType,EntityKeyPrefix from TopicAssignment where TopicId In (Select Id From Topic Where Name =:topicName )]){
                feedItemIds.add(t.EntityId);
            }
            
           
        
        }
        
         Public FeedItem[] getFeedItems() {
         System.debug('ID:  '+ feedItemIds);
         System.debug('typeofFeed  :  '+ typeofFeed );
         If(feedItemIds.size()>0 && typeofFeed !=null){
         return new List<FeedItem> ( [ Select Id, Body, Parent.Type From FeedItem Where ID IN : feedItemIds /*and Parent.Type =: typeofFeed*/ ] );
         }
         return null;
         }




}