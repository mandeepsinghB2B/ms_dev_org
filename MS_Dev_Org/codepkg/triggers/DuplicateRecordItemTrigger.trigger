trigger DuplicateRecordItemTrigger on DuplicateRecordItem (after insert) { 

System.debug('UserDebug: After Insert Duplicate Record Item'); 
for(DuplicateRecordItem dri: Trigger.new){ 
Lead currentLead = [select id, company from lead where id=:dri.RecordId]; 
Account currentAccount = [select id, ownerid from account where name=:currentLead.company]; 
LeadConvert.FutureFunction(currentLead.id, currentAccount.id,currentAccount.ownerid); 
} 
}